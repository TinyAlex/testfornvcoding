﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TestForNVCoding.Models
{
    public class TestForNVCodingContext : DbContext
    {
  
        public TestForNVCodingContext() : base("name=TestForNVCodingContext")
        {
        }

        public System.Data.Entity.DbSet<TestForNVCoding.Models.UserProfiles> UserProfiles { get; set; }
    }
}
