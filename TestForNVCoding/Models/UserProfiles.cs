﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestForNVCoding.Helpers;

namespace TestForNVCoding.Models
{
    public class UserProfiles
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
        [Required]
        [StringLength(int.MaxValue, MinimumLength = 6)]
        [DataType(DataType.Text)]
        public string Name { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DataType(DataType.Upload)]
        public string Avatar { get; set; }
        [SkypeValidate]
        public string Skype { get; set; }
        [DataType(DataType.Upload)]
        public string Signature { get; set; }
        
    }
}