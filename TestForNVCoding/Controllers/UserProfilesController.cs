﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TestForNVCoding.Helpers;
using TestForNVCoding.Models;

namespace TestForNVCoding.Controllers
{
    public class UserProfilesController : Controller
    {
        private TestForNVCodingContext db = new TestForNVCodingContext(); 
        // GET: UserProfile
        public async Task<ActionResult> Index()
        {
            return View(await db.UserProfiles.ToListAsync());
        }
        /// <summary>
        /// Action для создания и редактирования UserProfiles
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> AddOrFix(int? id)
        {
            //Если id отсутствует то, создать, в противном случае изменить
            if (id == null)
            {
                //Если прийдётся заменять эту ветку, лучше использовать Ajax
                UserProfiles newUserProfile = new UserProfiles();
                newUserProfile.Name = "NameExample";
                newUserProfile.Email = "Email@example.com";
                db.UserProfiles.Add(newUserProfile);
                await db.SaveChangesAsync();
                return View(newUserProfile);
            }
            UserProfiles userProfile = await db.UserProfiles.FindAsync(id);
            if (userProfile == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView("_AddOrFix");
            return View(userProfile);
        } 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddOrFix([Bind(Include = "Id,Name,Email,Avatar,Skype,Signature")] UserProfiles userProfile, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                int userId;
                if (userProfile.Id != null)
                {
                    userId = userProfile.Id;
                }
                else
                {
                    userId = 0;// в папку с 0 будет сохранять все изображения с ошибкой
                }
                UploadAvatar(file, userId);
                db.UserProfiles.Add(userProfile);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(userProfile);
        }
        /// <summary>
        /// Ответ сервера на Ajax запрос, сохраняет юзера если он валидный
        /// </summary>
        /// <param name="userData"></param>
        [HttpPost]
        public void Fix(UserData userData)
        {
            try //На случай если пользователь сам попробует изменить запрос
            {
                //Работает только с Ajax
                if (Request.IsAjaxRequest())
                {
                    UserProfiles userProfile = db.UserProfiles.Find(userData.id);
                    userProfile.Name = userData.Name;
                    userProfile.Email = userData.Email;
                    userProfile.Avatar = UploadAvatar(userData.Avatar, userProfile.Id);
                    userProfile.Signature = userData.Signature;
                    userProfile.Skype = userData.Skype;
                    db.Entry(userProfile).State = EntityState.Modified;
                    db.SaveChangesAsync();
                }
            }
            catch { }
        }
        /// <summary>
        /// Сохраняет изображения(можно добавить валидацию, если будет необходимость), путь ~/Content/Images/UserAvatars/{id}/{fileName}
        /// </summary>
        /// <param name="file">загружаемый файл на сервер</param>
        /// <param name="userId">id UserProfiles</param>
        /// <returns></returns>
        [HttpGet]
        public string UploadAvatar(HttpPostedFileBase file, int userId)
        {

            if (file != null)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Server.MapPath("~/Content/Images/UserAvatars/" + userId.ToString() + "/");
                var filePathToSave = path + fileName;
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                file.SaveAs(filePathToSave);
                var stringToReturn = userId.ToString() + "/" + fileName;
                return stringToReturn;
            }
            return "";
        }
    }
}