﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestForNVCoding.Helpers
{
    /// <summary>
    /// Костыль для сохранения данных, что бы сохранять только путь для поля Avatar
    /// </summary>
    public class UserData
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public HttpPostedFileBase Avatar { get; set; }
        public string Skype { get; set; }
        public string Signature { get; set; }
    }
}