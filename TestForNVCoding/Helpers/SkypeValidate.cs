﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestForNVCoding.Helpers
{
    /// <summary>
    /// Валидация для Skype, можно редактировать по необходимости
    /// </summary>
    public class SkypeValidateAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value != null)
            {
                if ((value as string).Length < 6 || (value as string).Length > 16)
                {
                    return false;
                }
            }
            return true;
        }
    }
}