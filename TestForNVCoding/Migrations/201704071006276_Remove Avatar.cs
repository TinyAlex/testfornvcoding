namespace TestForNVCoding.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveAvatar : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.UserProfiles", "Avatar");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfiles", "Avatar", c => c.String());
        }
    }
}
