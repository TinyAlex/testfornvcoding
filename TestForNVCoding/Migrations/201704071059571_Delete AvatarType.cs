namespace TestForNVCoding.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteAvatarType : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.UserProfiles", "AvatarType");
            AddColumn("dbo.UserProfiles", "AvatarType", c => c.String());
            AlterColumn("dbo.UserProfiles", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.UserProfiles", "Avatar", c => c.Binary());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserProfiles", "Avatar", c => c.String());
            AlterColumn("dbo.UserProfiles", "Name", c => c.String(nullable: false, maxLength: 50));
            DropColumn("dbo.UserProfiles", "AvatarType");
        }
    }
}
