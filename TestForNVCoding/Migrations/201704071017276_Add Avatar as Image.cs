namespace TestForNVCoding.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAvatarasImage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfiles", "Avatar", c => c.Binary());
            AddColumn("dbo.UserProfiles", "AvatarType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfiles", "Avatar");
            DropColumn("dbo.UserProfiles", "AvatarType");
        }
    }
}
