// <auto-generated />
namespace TestForNVCoding.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddAvatarasImage : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddAvatarasImage));
        
        string IMigrationMetadata.Id
        {
            get { return "201704071017276_Add Avatar as Image"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
