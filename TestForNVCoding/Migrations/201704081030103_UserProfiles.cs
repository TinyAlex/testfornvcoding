namespace TestForNVCoding.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserProfiles : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserProfiles", "Avatar", c => c.String());
            DropColumn("dbo.UserProfiles", "AvatarType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfiles", "AvatarType", c => c.String());
            AlterColumn("dbo.UserProfiles", "Avatar", c => c.Binary());
        }
    }
}
