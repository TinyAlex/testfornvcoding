﻿$(document).ready(function () {
    
    $(".form-control").change(function () {
        var formData = new FormData();
        formData.append('id', $("#userId").val());
        formData.append('Name', $("#Name").val());
        formData.append('Email', $("#Email").val());
        formData.append('Skype', $("#Skype").val());
        formData.append('Signature', $("#Signature").val());
        formData.append('Avatar', $("#Avatar").get(0).files[0]);
        $('#form0').validate({
            rules: {
                id: "required",
                Name: { "required": true, minlength: 6 },
                Email: { "required": true, email: true },
                Skype: { minlength: 6, maxlength: 16 }
            },
            messages:{
                id: "Error",
                Name: { required: "Please enter a name", minlength: 'Your name must consist of at least 6 characters' },
                Email: { required: "Please enter a name", email: "Please enter a valid email address" },
                Skype: { maxlength: "Your name must consist no more then 16 characters", minlength: 'Your name must consist of at least 6 characters' }
            }
        });
    $.ajax({
        type: "POST",
        url: '/UserProfiles/Fix',
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false
    });
    });
});  